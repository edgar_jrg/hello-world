#!/bin/bash
PROJECT_NAME=$(basename $(pwd))
PROYECT=${PWD##*/..}
clear
OPTION=$1
if [ -z ${OPTION} ]; then
echo "PROJECT NAME -> " $PROJECT_NAME
echo "ROUTE ->" $PROYECT
echo
echo "What do you need? (Select an option)"
echo
echo "1.- [Build a new instance]"
echo "2.- [Run the project]"
echo "0.- [Exit]"
echo
echo "Input your choice: "
read -n 2 OPTION
echo
fi
echo "Selected option $OPTION"
echo 

if [[ $OPTION = "1" ]] ;then
    clear
    echo "Building the project ...."
    rm -rf ./node_modules
    yarn install --prefer-offline
    docker rm -f $PROJECT_NAME-prod
    docker build -t $PROJECT_NAME-prod .
elif [[ $OPTION = "2" ]] ;then
    clear
    echo "Running the project ...."
    docker run -it --name $PROJECT_NAME-prod -p 3000:3000 --rm $PROJECT_NAME-prod
else
    clear
    echo "bye..."
fi
