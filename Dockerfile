FROM node:8.7

ENV APP_HOME /usr/src/app
WORKDIR ${APP_HOME}
COPY . ${APP_HOME}
RUN yarn install

CMD npm start
EXPOSE 3000