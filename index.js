const Koa = require('koa');
const app = new Koa();
const PORT = process.env.PORT || 3000;
app.use(async ctx => {
  ctx.body = 'Hello beautiful';
});

app.listen(PORT, _=> console.log(`listening on AWS port ${PORT}`));